import React from 'react';
import { v1 as uuid } from 'uuid';

const CreateRoom = ({ history }) => {
  const create = () => {
    const id = uuid();
    history.push(`/room/${id}`);
  }

  return (
    <header className="App-header">
      <button onClick={create}>Create Room</button>
    </header>
  );
}

export default CreateRoom;