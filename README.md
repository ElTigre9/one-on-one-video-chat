# One on One Video Chat

## What's in it?

Made with Express, socket.io, webRTC, and React (among other small libraries).

### What does it do?

Allows users to create a video chat room where another user can join (only 1 user).